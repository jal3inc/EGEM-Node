#!/bin/bash
echo "Updating linux packages"
apt-get update && apt-get upgrade -y
echo "Installing build-essential"
apt-get install -y build-essential
echo "Intalling screen"
apt install screen
echo "Installing git"
apt install git -y
echo "Install go-lang"
wget https://dl.google.com/go/go1.10.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.10.linux-amd64.tar.gz
mkdir -p ~/go; echo "export GOPATH=$HOME/go" >> ~/.bashrc
echo "export PATH=$PATH:$HOME/go/bin:/usr/local/go/bin" >> ~/.bashrc && source ~/.bashrc
echo "export GOROOT=/usr/local/go"
#echo "export PATH=$GOPATH/bin:$GOROOT/bin:$PATH"
echo "Cloning go-egem Repo"
git clone https://github.com/TeamEGEM/go-egem.git
echo "Cloning EGEM Net-Intelligence"
git clone https://github.com/TeamEGEM/egem-net-intelligence-api.git
echo "Configuring EGEM Node Details"

cd ~/go-egem && make egem
cd ~/go-egem && screen -dmS go-egem /root/go-egem/build/bin/egem --datadir ~/live-net/ --rpc


echo "What woud you like to name your instance? (Example: TeamEGEM Node East Coast USA)"
read INSTANCE_NAME

echo "What is your node's contact details? (Example: Twitter:@TeamEGEM)"
read CONTACT_DETAILS
rm -r ~/egem-net-intelligence-api/app.json
mv node-install/app.json ~/egem-net-intelligence-api/app.json

sed -i -e 's/NAME/'$INSTANCE_NAME'/g' ~/egem-net-intelligence-api/app.json
sed -i -e 's/CONTACT/'$CONTACT_DETAILS'/g' ~/egem-net-intelligence-api/app.json

 sleep 30 
 COUNTER=30
         until [  $COUNTER -lt 10 ]; do
             echo COUNTER $COUNTER
             let COUNTER-=1
         done 
echo "Done your node should be listed on https://network.egem.io"

